package com.example.android.galamatsystems;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


public class Second extends ActionBarActivity {
    Intent intent;
    int l = 0;
    String names[], username = "";
    JSONArray array;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        intent = getIntent();
        String js = intent.getStringExtra("JsonArray");
        username = intent.getStringExtra("username");
        try {
            array = new JSONArray(js);
            names = new String[array.length()];
            for(int i=0; i<array.length();i++){
                JSONArray ja = (JSONArray) array.get(i);
                names[l++] = "" + ja.get(0) + "\n" + ja.get(1) + "\n" + ja.get(2);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final ListView lvMain = (ListView) findViewById(R.id.lvMain);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, names);
        lvMain.setAdapter(adapter);
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    JSONArray jarr = (JSONArray) array.get(position);
                    names[position]=jarr.get(0) + "\n" + jarr.get(1) + "\n" + "yes";
                    adapter.notifyDataSetChanged();
                    lvMain.setAdapter(adapter);
                    new aTask().execute(position+"");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }
    class aTask extends AsyncTask<String, Void, String> {
        String url = "http://telegrambot.kz/android/mrX/index.php";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String id = params[0];
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            String responseString = "";
            try {
                ArrayList<NameValuePair> form = new ArrayList();
                form.add(new BasicNameValuePair("username", username));
                form.add(new BasicNameValuePair("success", id));
                post.setEntity(new UrlEncodedFormEntity(form));
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                responseString = EntityUtils.toString(entity, "UTF-8");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
        }
    }

}
