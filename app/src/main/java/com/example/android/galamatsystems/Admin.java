package com.example.android.galamatsystems;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Admin extends ActionBarActivity implements View.OnClickListener {
    String names[], names2[], names1[], namesToday1[], namesTomorrow1[], namesToday2[], namesTomorrow2[], namesLate1[], namesLate2[];
    Intent intent;
    Button btnAdd, btnToday, btnTomorrow, btnLate;
    JSONArray array, array2;
    ArrayAdapter<String> adapter;
    Date todayDate;
    Date date;
    int l2, l1, today = 0, tomorroww = 0, late = 0;
    String url = "http://telegrambot.kz/android/mrX/index.php";
    ListView lvMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setContentView(R.layout.activity_admin);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnToday = (Button) findViewById(R.id.btnToday);
        btnTomorrow = (Button) findViewById(R.id.btnTomorrow);
        btnLate = (Button) findViewById(R.id.btnLate);
        btnAdd.setOnClickListener(this);
        btnToday.setOnClickListener(this);
        btnTomorrow.setOnClickListener(this);
        btnLate.setOnClickListener(this);

        lvMain = (ListView) findViewById(R.id.lvMain);
        intent = getIntent();

        String s1 = intent.getStringExtra("user1");
        String s2 = intent.getStringExtra("user2");
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c.getTime());


        c.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = c.getTime();
        String tomorrowAsString = df.format(tomorrow);
        Toast.makeText(this, tomorrowAsString, Toast.LENGTH_SHORT).show();

        try {
            array = new JSONArray(s1);
            names1 = new String[array.length()];
            l1 = 0;
            today = 0;
            tomorroww = 0;
            late = 0;
            for (int i = 0; i < array.length(); i++) {
                JSONArray ja = (JSONArray) array.get(i);
                names1[l1++] = "" + ja.get(0) + "\n" + ja.get(1) + "\n" + ja.get(2);

                if (formattedDate.equals(ja.get(1))) {
                    today++;
                } else if (tomorrowAsString.equals(ja.get(1))) {
                    tomorroww++;
                } else if (fun("" + formattedDate, "" + ja.get(1))) {
                    late++;
                }
            }
            namesToday1 = new String[today];
            namesTomorrow1 = new String[tomorroww];
            namesLate1 = new String[late];
            late = 0;
            today = 0;
            tomorroww = 0;
            l1 = 0;
            for (int i = 0; i < array.length(); i++) {
                JSONArray ja = (JSONArray) array.get(i);
                names1[l1++] = "user1 | " + ja.get(0) + "\n" + ja.get(1) + "\n" + ja.get(2);
                if (formattedDate.equals(ja.get(1))) {
                    namesToday1[today++] = "user1 | " + ja.get(0) + "\n" + ja.get(1) + "\n" + ja.get(2);
                    ;
                } else if (tomorrowAsString.equals(ja.get(1))) {
                    namesTomorrow1[tomorroww++] = "user1 | " + ja.get(0) + "\n" + ja.get(1) + "\n" + ja.get(2);
                } else if (fun("" + formattedDate, "" + ja.get(1))) {
                    namesLate1[late++] = "user1 | " + ja.get(0) + "\n" + ja.get(1) + "\n" + ja.get(2);
                } else if (fun("" + formattedDate, "" + ja.get(1))) {
                    late++;
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            array2 = new JSONArray(s2);
            names2 = new String[array2.length()];
            l2 = 0;
            today = 0;
            tomorroww = 0;
            late = 0;
            for (int i = 0; i < array2.length(); i++) {
                JSONArray ja = (JSONArray) array2.get(i);
                names2[l2++] = "user2 | " + ja.get(0) + "\n" + ja.get(1) + "\n" + ja.get(2);
                if (formattedDate.equals(ja.get(1))) {
                    today++;
                } else if (tomorrowAsString.equals(ja.get(1))) {
                    tomorroww++;
                } else if (fun("" + formattedDate, "" + ja.get(1))) {
                    late++;
                }
            }
            namesToday2 = new String[today];
            namesTomorrow2 = new String[tomorroww];
            namesLate2 = new String[late];

            l2 = 0;
            today = 0;
            tomorroww = 0;
            late = 0;
            for (int i = 0; i < array2.length(); i++) {
                JSONArray ja = (JSONArray) array2.get(i);
                names2[l2++] = "user2 | " + ja.get(0) + "\n" + ja.get(1) + "\n" + ja.get(2);
                if (formattedDate.equals(ja.get(1))) {
                    namesToday2[today++] = "user2 | " + ja.get(0) + "\n" + ja.get(1) + "\n" + ja.get(2);
                    ;
                } else if (tomorrowAsString.equals(ja.get(1))) {
                    namesTomorrow2[tomorroww++] = "user2 | " + ja.get(0) + "\n" + ja.get(1) + "\n" + ja.get(2);
                } else if (fun("" + formattedDate, "" + ja.get(1))) {
                    namesLate2[late++] = "user2 | " + ja.get(0) + "\n" + ja.get(1) + "\n" + ja.get(2);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        names = new String[array.length() + array2.length()];
        l1 = array.length();
        l2 = array2.length();
        for (int i = 0; i < l1; i++) {
            names[i] = names1[i];
        }
        for (int i = l1; i < l1 + l2; i++) {
            names[i] = names2[i - l1];
        }
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, names);
        lvMain.setAdapter(adapter);

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAdd:
                Intent intent = new Intent(Admin.this, ActivityAdd.class);
                startActivity(intent);
                break;
            case R.id.btnToday:
                names = new String[namesToday1.length + namesToday2.length];
                for (int i = 0; i < namesToday1.length; i++) {
                    names[i] = namesToday1[i];
                }
                for (int i = namesToday1.length; i < namesToday1.length + namesToday2.length; i++) {
                    names[i] = namesToday2[i - namesToday1.length];
                }
                adapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, names);
                lvMain.setAdapter(adapter);
                break;

            case R.id.btnTomorrow:
                names = new String[namesTomorrow1.length + namesTomorrow2.length];
                for (int i = 0; i < namesTomorrow1.length; i++) {
                    names[i] = namesTomorrow1[i];
                }
                for (int i = namesTomorrow1.length; i < namesTomorrow1.length + namesTomorrow2.length; i++) {
                    names[i] = namesTomorrow2[i - namesTomorrow1.length];
                }
                adapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, names);
                lvMain.setAdapter(adapter);
                break;
            case R.id.btnLate :
                names = new String[namesLate1.length + namesLate2.length];
                for (int i = 0; i < namesLate1.length; i++) {
                    names[i] = namesLate1[i];
                }
                for (int i = namesLate1.length; i < namesLate1.length + namesLate2.length; i++) {
                    names[i] = namesLate2[i - namesLate1.length];
                }
                adapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, names);
                lvMain.setAdapter(adapter);
                break;
        }
    }

    public boolean fun(String today, String deadline) {
        String t[] = today.split("-");
        String d[] = deadline.split("-");

        if (Integer.parseInt(t[2])<=Integer.parseInt(d[2])){
            if(Integer.parseInt(t[1])<=Integer.parseInt(d[1])){
                if(Integer.parseInt(t[0])<=Integer.parseInt(d[0])){
                    return false;
                }
                return true;
            }
            else return true;
        }
        else{
            return true;
        }
    }
}

