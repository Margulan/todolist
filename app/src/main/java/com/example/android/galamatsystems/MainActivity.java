package com.example.android.galamatsystems;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    EditText etLogin, etPassword;
    Button btnLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etLogin = (EditText) findViewById(R.id.etLogin);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
    }

    public void onClick(View view) {
        new myTask().execute(etLogin.getText().toString(), etPassword.getText().toString());
    }

    class myTask extends AsyncTask <String, Void, String> {

        String url = "http://telegrambot.kz/android/mrX/index.php";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String log = params[0];
            String pass = params[1];
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            String responseString = "";
            try {
                ArrayList<NameValuePair> form = new ArrayList();
                form.add(new BasicNameValuePair("login", log));
                form.add(new BasicNameValuePair("password", pass));
                post.setEntity(new UrlEncodedFormEntity(form));

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                responseString = EntityUtils.toString(entity, "UTF-8");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            try {
                JSONObject jObj = new JSONObject(json);
                if(jObj.getString("type").equals("user")) {
                    JSONArray arr = jObj.getJSONArray(jObj.getString("username"));
                    Intent intent = new Intent(MainActivity.this, Second.class);
                    intent.putExtra("JsonArray", arr.toString());
                    intent.putExtra("username", jObj.getString("username"));
                    startActivity(intent);
                } else if (jObj.getString("type").equals("admin")) {
                    Intent intent = new Intent(MainActivity.this, Admin.class);
                    JSONArray arr = jObj.getJSONArray("user1");
                    JSONArray arr2 = jObj.getJSONArray("user2");
                    intent.putExtra("user1", arr.toString());
                    intent.putExtra("user2", arr2.toString());
                    startActivity(intent);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
