package com.example.android.galamatsystems;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class ActivityAdd extends ActionBarActivity implements View.OnClickListener {
    Button btnSave;
    EditText etTask, etDeadline, etUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        btnSave = (Button) findViewById(R.id.btnSave);
        etTask = (EditText) findViewById(R.id.etTask);
        etDeadline = (EditText) findViewById(R.id.etDeadline);
        etUser = (EditText) findViewById(R.id.etUser);

        btnSave.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        new addTask().execute(etTask.getText().toString(), etDeadline.getText().toString(), etUser.getText().toString());
    }

    class addTask extends AsyncTask<String, Void, String> {

        String url = "http://telegrambot.kz/android/mrX/index.php";

        @Override
        protected String doInBackground(String... params) {
            String sTask = params[0];
            String sDeadline = params[1];
            String sUser = params[2];

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            String responseString = "";
            try {
                ArrayList<NameValuePair> form = new ArrayList();
                form.add(new BasicNameValuePair("task", sTask));
                form.add(new BasicNameValuePair("deadline", sDeadline));
                form.add(new BasicNameValuePair("adduser", sUser));
                post.setEntity(new UrlEncodedFormEntity(form));

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                responseString = EntityUtils.toString(entity, "UTF-8");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            Log.d("mylogs", "sdasda");
            Intent in = new Intent(ActivityAdd.this, MainActivity.class);
            startActivity(in);

        }
    }
}
